import requests

import properties
from business_object.brasseries import Brasseries
from daobrasseries import DaoBrasseries


if __name__ == "__main__":
    dao = DaoBrasseries()

    print('------------------------------------------------------------------- \n')
    try:
        proxies = {
            'http': properties.http_proxy,
            'https': properties.https_proxy
            }
# On prépare les params de la requête. C'est requests qui va les mettre en forme pour nous !
        params = {
            'key': properties.api_key}

        response = requests.get(
            'http://api.brewerydb.com/v2/breweries/'
            , proxies=proxies
            , params=params)

        datas = response.json()['data']
        result = {}
        for row in datas:
            # la première ligne va servir pour les clés de nos dictionnaires pythons
            brasserie = row['id']
            try:
                result[brasserie] = {'nom':row['name']}
                a = row['name']
            except:
                result[brasserie] = {'nom':""}
                a = ""
            try:
                result[brasserie]['description']=row['description']
                b = row['description']
            except:
                result[brasserie]['description']=""
                b = ""
            try:
                result[brasserie]['date_creation']=row['createDate']
                c = row['createDate']
            except:
                result[brasserie]['date_creation'] = ""
                c = ""
            try:
                result[brasserie]['date_modification']=row['updateDate']
                d = row['updateDate']
            except:
                result[brasserie]['date_modification']=""
                d = ""
            dao.ajouter_brasserie(brasserie,a,b,c,d)
        print('données insérées en base \n')
        print(
            '------------------------------------------------------------------- \n')
    except requests.exceptions.RequestException as error:
        print(error)

