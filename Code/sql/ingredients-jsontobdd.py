import requests

import properties
from daoingredients import DaoIngredients

if __name__ == "__main__":
    dao = DaoIngredients()
    print('------------------------------------------------------------------- \n')
    try:
        proxies = {
            'http': properties.http_proxy,
            'https': properties.https_proxy
            }
        result = {}
        for i in range(1,4,1):
# On prépare les params de la requête. C'est requests qui va les mettre en forme pour nous !
            params = {
                'p':  i,
                'key': properties.api_key}

            # On envoie une requête pour récupérer les statistiques d'emprunt de films
            response = requests.get(
                'https://sandbox-api.brewerydb.com/v2/ingredients/'
                , proxies=proxies
                , params=params)

            datas = response.json()['data']
            # est-ce que l'entête du fichier à été écrit
            for row in datas:
                # la première ligne va servir pour les clés de nos dictionnaires pythons
                id = row['id']
                try:
                    result[id] = {'nom':row['name']}
                    a = row['name']
                except:
                    result[id] = {'nom':""}
                    a = ""
                try:
                    result[id]['categorie']=row['category']
                    b = row['category']
                except:
                    result[id]['categorie']=""
                    b = ""
                dao.ajouter_ingredients(id,a,b)
            print('données insérées en base \n')
            print(
                '------------------------------------------------------------------- \n')
    except requests.exceptions.RequestException as error:
        print(error)