import requests

import properties
from business_object.events import Events
from daoevents import DaoEvents


if __name__ == "__main__":
    dao = DaoEvents()

    print('------------------------------------------------------------------- \n')
    try:
        proxies = {
            'http': properties.http_proxy,
            'https': properties.https_proxy
            }
# On prépare les params de la requête. C'est requests qui va les mettre en forme pour nous !

        for i in range(1,3):
            params = {
                'key': properties.api_key,
                'p': i}
            response = requests.get(
                'http://api.brewerydb.com/v2/events/'
                , proxies=proxies
                , params=params)

            datas = response.json()['data']
            # est-ce que l'entête du fichier à été écrit
            result = {}
            for row in datas:
                # la première ligne va servir pour les clés de nos dictionnaires pythons
                event = row['id']
                try:
                    result[event] = {'year':row['year']}
                    a = row['year']
                except:
                    result[event] = {'year':""}
                    a = ""
                try:
                    result[event]['name']=row['name']
                    b = row['name']
                except:
                    result[event]['name']=""
                    b = ""
                try:
                    result[event]['description']=row['description']
                    c = row['description']
                except:
                    result[event]['description'] = ""
                    c = ""
                try:
                    result[event]['locality']=row['locality']
                    d = row['locality']
                except:
                    result[event]['locality']=""
                    d = ""

                try:
                    result[event]['startDate'] = row['startDate']
                    e = row['startDate']
                except:
                    result[event]['startDate'] = ""
                    e = ""

                try:
                    result[event]['type'] = row['type']
                    f = row['type']
                except:
                    result[event]['type'] = ""
                    f = ""
                ev=Events(event,a,b,c,d,e,f)
                dao.ajouter(ev)
        print('données insérées en base \n')
        print(
            '------------------------------------------------------------------- \n')
    except requests.exceptions.RequestException as error:
        print(error)

