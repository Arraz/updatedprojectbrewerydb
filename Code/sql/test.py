import requests

import properties
from business_object.bieres import Bieres
from daostructure.daobieres import DaoBieres


if __name__ == "__main__":
    dao = DaoBieres()

    print('------------------------------------------------------------------- \n')
    try:
        proxies = {
            'http': properties.http_proxy,
            'https': properties.https_proxy
            }
# On prépare les params de la requête. C'est requests qui va les mettre en forme pour nous !
        params = {
            'key': properties.api_key}

        # On envoie une requête poru récupérer les statistiques d'emprunt de films
        response = requests.get(
            'http://api.brewerydb.com/v2/beers/'
            , proxies=proxies
            , params=params)

        datas = response.json()
    except requests.exceptions.RequestException as error:
        print(error)
print(datas)