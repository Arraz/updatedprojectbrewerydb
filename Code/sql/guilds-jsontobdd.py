import requests

import properties
from daoguildes import DaoGuild


if __name__ == "__main__":
    dao = DaoGuild()

    print('------------------------------------------------------------------- \n')
    try:
        proxies = {
            'http': properties.http_proxy,
            'https': properties.https_proxy
            }
# On prépare les params de la requête. C'est requests qui va les mettre en forme pour nous !
        params = {
            'key': properties.api_key}

        # On envoie une requête poru récupérer les statistiques d'emprunt de films
        response = requests.get(
            'http://api.brewerydb.com/v2/guilds/'
            , proxies=proxies
            , params=params)

        datas = response.json()['data']
        # est-ce que l'entête du fichier à été écrit
        result = {}
        for row in datas:
            # la première ligne va servir pour les clés de nos dictionnaires pythons
            guilde = row['id']
            try:
                result[guilde] = {'nom':row['name']}
                a = row['name']
            except:
                result[guilde] = {'nom':""}
                a = ""
            try:
                result[guilde]['description']=row['description']
                b = row['description']
            except:
                result[guilde]['description']=""
                b = ""
            try:
                result[guilde]['date_fondation']=row['established']
                c = row['established']
            except:
                result[guilde]['date_fondation'] = ""
                c = ""
            dao.ajouter_guilde(guilde,a,b,c)
        print('données inserees en base \n')
        print(
            '------------------------------------------------------------------- \n')
    except requests.exceptions.RequestException as error:
        print(error)