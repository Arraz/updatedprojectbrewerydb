
import requests

import properties
from daobrasseriesguildes import DaoBrasseriesGuildes
from guild_list import Guildeliste


if __name__ == "__main__":
    D_b_g = DaoBrasseriesGuildes()
    b_g = Guildeliste()
    print('------------------------------------------------------------------- \n')
    try:
        proxies = {'http': properties.http_proxy,'https':properties.https_proxy}
        params = {'key': properties.api_key}
        r1 = requests.get('http://api.brewerydb.com/v2/breweries/', proxies=proxies, params=params)
        brasseries = r1.json()['data']
        for b in brasseries:
            brasserie = b['id']
            r2 = requests.get('http://api.brewerydb.com/v2/brewery/%s/guilds' %brasserie, proxies=proxies, params=params)
            try:
                guildes = r2.json()['data']
                for g in guildes:
                    guilde = g['id']
                    b_g.associer(brasserie ,guilde)
            except:
                pass
        print('données insérées en base \n')
        print(
            '------------------------------------------------------------------- \n')
    except requests.exceptions.RequestException as error:
        print(error)