import requests
import properties
from daobieresbrasseries import DaoBieresBrasseries


if __name__ == "__main__":
    dao = DaoBieresBrasseries()

    print('------------------------------------------------------------------- \n')
    try:
        dao = DaoBieresBrasseries()
        proxies = {
            'http': properties.http_proxy,
            'https': properties.https_proxy
            }
        result = {}
        for i in range(1,24,1):
# On prépare les params de la requête. C'est requests qui va les mettre en forme pour nous !
            params = {
                'p':  i,
                'key': properties.api_key,
                'withBreweries':'Y'}

            # On envoie une requête pour récupérer les statistiques d'emprunt de films
            response = requests.get(
                'http://api.brewerydb.com/v2/beers/'
                , proxies=proxies
                , params=params)

            datas = response.json()['data']
            # est-ce que l'entête du fichier à été écrit
            for row in datas:
                # la première ligne va servir pour les clés de nos dictionnaires pythons
                bierebrasserie = row['id']
                a = row['breweries'][0]['id']
                result[bierebrasserie] = row['breweries'][0]['id']
                dao.ajouter(bierebrasserie,a)
            print('donnees inserees en base \n')
            print(
                '------------------------------------------------------------------- \n')
    except requests.exceptions.RequestException as error:
        print(error)

