import requests

import properties
from daobieres import DaoBieres


if __name__ == "__main__":
    dao = DaoBieres()

    print('------------------------------------------------------------------- \n')
    try:
        dao = DaoBieres()
        proxies = {
            'http': properties.http_proxy,
            'https': properties.https_proxy
            }
        result = {}
        for i in range(1,24,1):
# On prépare les params de la requête. C'est requests qui va les mettre en forme pour nous !
            params = {
                'p':  i,
                'key': properties.api_key}

            # On envoie une requête pour récupérer les statistiques d'emprunt de films
            response = requests.get(
                'http://api.brewerydb.com/v2/beers/'
                , proxies=proxies
                , params=params)

            datas = response.json()['data']
            # est-ce que l'entête du fichier à été écrit
            for row in datas:
                # la première ligne va servir pour les clés de nos dictionnaires pythons
                biere = row['id']
                try:
                    result[biere] = {'nom':row['name']}
                    a0 = row['name']
                except:
                    result[biere] = {'name':""}
                    a0 = ""
                try:
                    result[biere] = {'label':row['labels']['medium']}
                    a = row['labels']['medium']
                except:
                    result[biere] = {'label':""}
                    a = ""
                try:
                    result[biere]['description']=row['style']['description']
                    b = row['style']['description']
                except:
                    result[biere]['description']=""
                    b = ""
                try:
                    result[biere]['styleid']=row['style']['id']
                    c = row['style']['id']
                except:
                    result[biere]['styleid'] = ""
                    c = ""
                try:
                    result[biere]['categoryid']=row['style']['categoryId']
                    d = row['style']['categoryId']
                except:
                    result[biere]['categoryid']=""
                    d = ""
                dao.ajouter_biere(biere,a0,a,b,c,d)
            print('données insérées en base \n')
            print(
                '------------------------------------------------------------------- \n')
    except requests.exceptions.RequestException as error:
        print(error)

