import requests

import properties
from daobieres import DaoBieres
from daoingredients import DaoIngredients


if __name__ == "__main__":
    dao = DaoBieres()
    daoI = DaoIngredients()
    print('------------------------------------------------------------------- \n')
    try:
        ids = dao.importer_id()
        proxies = {
            'http': properties.http_proxy,
            'https': properties.https_proxy
            }
        result = {}
# On prépare les params de la requête. C'est requests qui va les mettre en forme pour nous !
        params = {'key': properties.api_key}
        for id in ids:
            # On envoie une requête pour récupérer les statistiques d'emprunt de films
            response = requests.get(
                'https://sandbox-api.brewerydb.com/v2/beer/%s/ingredients' % id
                , proxies=proxies
                , params=params)

            try :
                datas = response.json()['data']
                # est-ce que l'entête du fichier à été écrit
                for row in datas:
                    # la première ligne va servir pour les clés de nos dictionnaires pythons
                    id_ingredient=row['id']
                    daoI.ajouter_biere_ingredient(id,id_ingredient)
                print('données insérées en base \n')
                print(
                    '------------------------------------------------------------------- \n')
            except:
                pass
    except requests.exceptions.RequestException as error:
        print(error)
