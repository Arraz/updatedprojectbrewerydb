import requests

import properties
from daoevents import DaoEvents
from daobieres import DaoBieres
from daobieresevents import DaoEvenementBiere

if __name__ == "__main__":
    dao = DaoBieres()
    daoE = DaoEvents()
    daoevenementbiere= DaoEvenementBiere()
    print('------------------------------------------------------------------- \n')
    try:
        ids = daoE.importer_id()
        proxies = {
            'http': properties.http_proxy,
            'https': properties.https_proxy
            }
        try:
            for i in range(1,3):
                params = {'key': properties.api_key,
                          'p' : i}
                for id in ids:
                    # On envoie une requête pour récupérer les statistiques d'emprunt de films
                    response = requests.get(
                        'https://sandbox-api.brewerydb.com/v2/event/%s/beers/' % id[0]
                        , proxies=proxies
                        , params=params)

                    try :
                        datas = response.json()['data']
                        # est-ce que l'entête du fichier à été écrit
                        for row in datas:
                            # la première ligne va servir pour les clés de nos dictionnaires pythons
                            id_beer=row['id']
                            daoevenementbiere.ajouter(id,id_beer)
                        print('données insérées en base \n')
                        print(
                            '------------------------------------------------------------------- \n')
                    except:
                        pass
        except:
            pass
    except requests.exceptions.RequestException as error:
        print(error)