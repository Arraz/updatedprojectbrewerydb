import requests

import properties
from business_object.category import Category
from daostructure.daocategory import DaoCategory


if __name__ == "__main__":
    dao = DaoCategory()

    print('------------------------------------------------------------------- \n')
    try:
        dao = DaoCategory()
        proxies = {
            'http': properties.http_proxy,
            'https': properties.https_proxy
            }
# On prépare les params de la requête. C'est requests qui va les mettre en forme pour nous !
        params = {
            'key': properties.api_key}

        # On envoie une requête poru récupérer les statistiques d'emprunt de films
        response = requests.get(
            'http://api.brewerydb.com/v2/categories/'
            , proxies=proxies
            , params=params)

        datas = response.json()['data']
        # est-ce que l'entête du fichier à été écrit
        result = {}
        for row in datas:
            # la première ligne va servir pour les clés de nos dictionnaires pythons
            category = row['id']
            try:
                result[category] = {'name':row['name']}
                a = row['name']
            except:
                result[category] = {'name':""}
                a = ""
            try:
                result[category]['createDate']=row['createDate']
                b = row['createDate']
            except:
                result[category]['createDate']=""
                b = ""

            cat=Category(category,a,b)
            dao.create(cat)
        print('données insérées en base \n')
        print(
            '------------------------------------------------------------------- \n')
    except requests.exceptions.RequestException as error:
        print(error)

