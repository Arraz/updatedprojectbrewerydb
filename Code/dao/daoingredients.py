from daostructure import Dao
import connection


class DaoIngredients:
    dao=Dao()
    def ajouter_ingredients(self,id,nom,categorie):
        scarac = "%s,%s,%s"
        carac = (id , nom , categorie)
        self.dao.ajouter("ingredients","id,nom,categorie",scarac,carac)

    def afficher_categorie_ingredient(self,categorie):
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("select * from ingredients where categorie= '%s' ",categorie)
                nvtableau = cur.fetchall()
                return (nvtableau)
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()

    def ajouter_biere_ingredient(self,id_biere,id_ingredient):
        ids=(id_biere,id_ingredient)
        scarac="%s,%s"
        self.dao.ajouter("biereingredients","id_biere, id_ingredient",scarac,ids)

    def afficher_toutes_categorie(self):
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("select distinct categorie from ingredients ")
                nvtableau = cur.fetchall()
                return (nvtableau)
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()