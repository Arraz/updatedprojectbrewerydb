import connection
from daostructure import Dao

class DaoCompte:
    dao = Dao()
    def compte_existant(self,ident): #Cette fonction permet de savoir si un compte existe ou non a partir d'un identifiant
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("select count(*) from account where id = %s", (ident,))
                r=cur.fetchall()
                connexion.commit()
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()
        return(r[0][0])

    def ajouter_compte(self,ident,mdp):
        carac= (ident,mdp)
        scarac="%s, %s"
        self.dao.ajouter("account","id, motdepasse",scarac,carac)

    def supprimer_compte(self,ident):
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("delete from account where id=%s", (ident,))
                connexion.commit()
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()
    def connexion(self,ident):
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("select motdepasse from account where id = %s", [ident])
                r=cur.fetchall()
                connexion.commit()
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()
        return(r[0][0])
        
