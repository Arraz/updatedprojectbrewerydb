import connection
from daostructure import Dao #On importe la structure de Dao classique dans le cadre de notre projet

class DaoBieres:
    dao=Dao()

    def rechercher_biere(self, criteres):
        # Traitement de la commande => commande sql correspondante
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try: # On fait notre requete sql en joignant les tables des bieres, des brasseries et des ingredients
                cur.execute("""select biere.id, biere.nom,biere.label,biere.description,biere.styleid,biere.categorieid 
                from biere join bieresbrasseries on biere.id=bieresbrasseries.id_biere 
join brasserie on brasserie.id=bieresbrasseries.id_brasserie
 join biereingredients on biere.id = biereingredients.id_biere
  join ingredients on biereingredients.id_ingredient=ingredients.id where """+criteres)
                nvtableau = cur.fetchall()
                return (nvtableau)
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()

    def ajouter_biere(self, id,nom,label,description,styleid,categorieid):
        biere = (id,nom, label, description, styleid, categorieid)
        scarac="%s,%s,%s,%s,%s,%s"
        self.dao.ajouter("biere","id,nom,label,description,styleid,categorieid",scarac,biere)

    def importer_biere(self):
        return self.dao.importer("biere")

    def afficher_biere_id(self, id):
        return self.dao.afficher("biere",id)[0]

    def afficher_biere_nom(self,nom):
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("select biere.id, biere.nom,biere.label,biere.description,biere.styleid,biere.categorieid from biere where nom = %s",(nom))
                nvtableau = cur.fetchall()
                return (nvtableau)
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()

    def supprimer_biere(self, id):
        self.dao.supprimer("biere",id)

    def importer_id(self):
        self.dao.importer_id("biere")


