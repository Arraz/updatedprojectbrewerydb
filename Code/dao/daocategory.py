import connection
from daostructure import Dao

class DaoCategory:
    dao= Dao()
    def create(self, category):
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute(
                    "insert into category( id, nom, date_creation) values (%s,%s,%s)", (category.id, category.nom, category.date_creation))
                connexion.commit()
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()
    def toutes_categories(self):
        return dao.importer("category")

