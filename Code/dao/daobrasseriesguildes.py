from daostructure import Dao
import connection

class DaoBrasseriesGuildes:
    dao=Dao()
    def importer(self):
        return self.dao.importer("brasseriesguildes")

    def ajouter(self,id_brasserie,id_guilde):
        ids=(id_brasserie,id_guilde)
        scarac="%s,%s"
        self.dao.ajouter("brasseriesguildes","id_brasserie, id_guilde",scarac,ids)
    def supprimer(self,id_brasserie,id_guilde):
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("delete from brasseriesguildes where id_brasserie=%s and id_guilde=%s", (str(id_brasserie),str(id_guilde)))
                connexion.commit()
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()


    def dissocier_tout_brasserie(self, id_brasserie):
        self.dao.supprimer("brasseriesguildes",str(id_brasserie))

    def dissocier_tout_guilde(self, id_guilde):
        self.dao.supprimer("brasseriesguildes",str(id_guilde))

