import connection
class Dao:

    def ajouter(self, nom_dao, nom_carac,scarac ,carac):
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute(
                    "insert into "+nom_dao+"(" +nom_carac+")"+ "values ("+scarac+")",carac)
                connexion.commit()
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()

    def afficher(self, nom_dao, id):
        # Traitement de la commande => commande sql correspondante
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("select * from "+nom_dao+" where id=%s", (id,))
                objet = cur.fetchall()
                return(objet)
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()

    def supprimer(self, nom_dao, id):
        # Traitement de la commande => commande sql correspondante
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("delete from "+nom_dao+" where id=%s", (id,))
                connexion.commit()
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()
    def importer(self, nom_dao):
        # Traitement de la commande => commande sql correspondante
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("select * from " + nom_dao)
                nvtableau = cur.fetchall()
                return (nvtableau)
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()

    def importer_id(self,nom_dao):
        # Traitement de la commande => commande sql correspondante
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("select id from " + nom_dao)
                nvtableau = cur.fetchall()
                return (nvtableau)
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()