import connection
from daostructure import Dao

class DaoGuild:
    dao = Dao()
    def ajouter_guilde(self, id, nom, description, date_fondation):
        carac=(id, nom, description, date_fondation)
        scarac="%s,%s,%s,%s"
        self.dao.ajouter("guildes","id, nom, description, date_fondation",scarac,carac)

    def afficher_guilde(self, id):
        return(self.dao.afficher("guildes",id)[0])

    def supprimer_guilde(self, id):
        self.dao.supprimer("guildes",id)

    def lier_brasserie_guilde(self,id_guilde,id_brasserie):
        carac = "%s, %s" % (id_guilde,id_brasserie)
        self.dao.ajouter("guildebrasserie","id_guilde, id_brasserie",carac)

    def import_id(self):
        return self.dao.importer_id("guildes")

    def importer(self):
        return self.dao.importer("guildes")

    def delier(self,id_guilde,id_brasserie):
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute(
                    "delete from guildebrasserie where id_guilde=%s, id_brasserie=%s", (id_guilde,id_brasserie))
                connexion.commit()
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()
