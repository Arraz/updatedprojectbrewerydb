import connection
from daostructure import Dao

class DaoEvents:
    dao=Dao()
    def ajouter(self, event):
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute(
                    "insert into event(id,annee, nom, description, lieu,date,type) values (%s,%s,%s,%s,%s,%s,%s)", (event.id, event.annee, event.nom, event.description, event.lieu, event.date,event.type))
                connexion.commit()
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()
    def importer(self):
        self.dao.importer("event")

    def importer_id(self):
            # Traitement de la commande => commande sql correspondante
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("select id from event")
                nvtableau = cur.fetchall()
                return (nvtableau)
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()

