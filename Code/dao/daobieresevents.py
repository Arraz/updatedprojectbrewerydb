from daostructure import Dao

class DaoEvenementBiere:
    dao=Dao()
    def importer(self):
        return self.dao.importer("eventbeers")

    def ajouter(self,id_event,id_biere):
        ids=(id_event,id_biere)
        scarac="%s,%s"
        self.dao.ajouter("bieresevents","id_event,id_biere ",scarac,ids)
