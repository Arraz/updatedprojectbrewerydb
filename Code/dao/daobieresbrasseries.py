from daostructure import Dao
import connection

class DaoBieresBrasseries:
    dao=Dao()
    def importer(self):
        return self.dao.importer("bieresbrasseries")

    def ajouter(self,id_biere,id_brasserie):
        ids=(id_biere,id_brasserie)
        scarac="%s,%s"
        self.dao.ajouter("bieresbrasseries","id_biere, id_brasserie",scarac,ids)

    def supprimer(self,id_biere):
        self.dao.supprimer("bieresbrasseries",id_biere)

    def nbbiereparbrasserie(self,id_brasserie):
            # Traitement de la commande => commande sql correspondante
        connexion = connection.get_connection()
        with connexion.cursor() as cur:
            try:
                cur.execute("select count(id_biere) from bieresbrasseries where id_brasserie = %s",(id_brasserie,))
                nvtableau = cur.fetchall()
                return (nvtableau)
            except ValueError as e:
                connexion.rollback()
                raise e
            finally:
                connexion.close()
