from daostructure import Dao

class DaoBrasseries:
    dao = Dao()
    def ajouter_brasserie(self, id, nom="", description="", date_creation="", date_modification=""):
        carac=(id, nom, description, date_creation, date_modification)
        scarac="%s,%s,%s,%s,%s"
        self.dao.ajouter("brasserie","id, nom, description, date_creation, date_modification",scarac,carac)

    def importe(self):
        return self.dao.importer("brasserie")

    def supprimer_brasserie(self, id):
        self.dao.supprimer("brasserie", id)


    def afficher_brasserie(self, id):
        return self.dao.afficher("brasserie", id)[0]

    def import_ids(self):
        return self.dao.importer_id("brasserie")

