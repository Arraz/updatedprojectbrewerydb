from daoingredients import DaoIngredients
from ingredient import Ingredients

class Ingredients_liste:
    daoingredient = DaoIngredients()
    listcategorieingredient = daoingredient.afficher_toutes_categorie()

    def afficher_ingredient_categorie(self):
        categorie = input("donnez une categorie d'ingredient ou ecrivez quitter: ")
        L = ["quitter"]
        for i in range(0, len(self.listcategorieingredient),1):# On cree une list contenant tout les ids et "quitter"
            L = L + [self.listcategorieingredient[i][0]]
        while (categorie) not in L:
            print("votre identifiant n'existe pas")
            categorie = input("categorie d'ingredient ou ecrivez quitter: ")
        if categorie != "quitter":
            L= self.daoingredient.afficher_categorie_ingredient(categorie)
        for i in L :
            Ingredient=Ingredients(*i)
            Ingredient.afficher()

    def afficher_toutes_categories(self):
        L = self.daoingredient.afficher_toutes_categorie()
        for c in L:
            print(c[0])



