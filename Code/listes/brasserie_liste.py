from daobrasseries import DaoBrasseries
from daobieresbrasseries import DaoBieresBrasseries
from daobrasseriesguildes import DaoBrasseriesGuildes
from daobieres import DaoBieres
from brasseries import Brasseries
import datetime
import pylab
import numpy as np
class Brasserieliste:
    daobierebrasserie = DaoBieresBrasseries()
    daobrasserie = DaoBrasseries()
    listbrasseries = DaoBrasseries().importe()
    listbiere = DaoBieres().importer_biere()
    listbierebrasserie = DaoBieresBrasseries().importer()
    listids=daobrasserie.import_ids()
    def ajouter_brasserie(self):
        m=1
        id_brasserie = input("identifiant de la brasserie (ecrivez quitter pour revenir au menu precedent) : ")
        while (id_brasserie in [self.listbrasseries[i][0] for i in
                                    range(0, len(self.listbrasseries) - 1, 1)]) and id_brasserie != "quitter":
            id_brasserie = input(
                "cet identifiant existe deja, ecrivez quitter pour revenir au menu ou donnez un identifiant non existant ")
        if id_brasserie == "quitter":
            m = 2
        if m ==1:
            label=input("donner le label de la brasserie, appuyez sur entrer si vous ne souhaitez pas renseigner cet element ")
            description=input("donner la description de la brasserie, appuyez sur entrer si vous ne souhaitez pas renseigner cet element ")
            date_creation = str(datetime.datetime.now())
            self.daobrasserie.ajouter_brasserie(id_brasserie,label,description,date_creation,"")
            print("la brasserie %s vient d'etre ajoutee" % id_brasserie)

    def supprimer_brasserie(self):
        listbrasseries=DaoBrasseries().importe()
        m = 1
        id_brasserie = input("Quelle brasserie souhaitez vous supprimer? ecrivez quitter pour revenir au menu precedent ")
        while ((id_brasserie) not in [listbrasseries[i][0] for i in
                                    range(0, len(listbrasseries) - 1, 1)]) and id_brasserie != "quitter":
            id_brasserie = input(
                    "cet identifiant n'existe pas, ecrivez quitter pour revenir au menu ou donnez un identifiant existant ")
        if id_brasserie == "quitter":
            m = 2
        if m == 1:
            self.daobrasserie.supprimer_brasserie(id_brasserie)
            print("la brasserie %s vient d'etre suprimee" % id_brasserie)

    def afficher_brasserie(self):
        m = 1
        id_brasserie = input("Quelle brasserie souhaitez vous afficher? ecrivez quitter pour revenir au menu precedent ")
        while ((id_brasserie) not in [self.listbrasseries[i][0] for i in range(0, len(self.listbrasseries) - 1, 1)]) and id_brasserie != "quitter":
            id_brasserie = input(
                "cet identifiant n'existe pas, ecrivez quitter pour revenir au menu ou donnez un identifiant existant ")
        if id_brasserie == "quitter":
            m = 2
        if m == 1:
            brasserie = Brasseries(*self.daobrasserie.afficher_brasserie(id_brasserie)[0:])
            brasserie.afficher()

    def ajouter_bierebrasserie(self):
        m = 1
        id_biere=input("Quelle bière souhaitez vous associer à une brasserie? ecrivez quitter pour revenir au menu precedent ")
        while ((id_biere not in [self.listbiere[i][0] for i in range (0,len(self.listbiere)-1,1)]) and id_biere != "quitter") or ((id_biere in [self.listbierebrasserie[i][0] for i in range (0,len(self.listbierebrasserie)-1,1)]) and id_biere != "quitter"):
            id_biere=input("cet identifiant n'existe pas ou est deja associe a une brasserie, ecrivez quitter pour revenir au menu ou donnez un identifiant existant ")
        if id_biere == "quitter":
            m = 2
        if m == 1:
            while m == 1:
                id_brasserie = input("A quelle brasserie souhaitez vous ajouter cette bière? ecrivez quitter pour revenir au menu precedent ")
                while ((id_brasserie) not in [self.listbrasseries[i][0] for i in
                                        range(0, len(self.listbrasseries) - 1, 1)]) and id_brasserie != "quitter":
                    id_brasserie = input(
                        "cet identifiant n'existe pas, ecrivez quitter pour revenir au menu ou donnez un identifiant existant ")
            if id_brasserie == "quitter":
                m = 2
        if m == 1:
            self.daobierebrasserie.ajouter(id_biere,id_brasserie)
            print("Vous venez d'ajouter la biere %s a la brasserie %s" % (id_biere,id_brasserie))

    def supprimer_bierebrasserie(self):
        m = 1
        id_biere=input("Quelle biere souhaitez vous enlever a sa brasserie? ecrivez quitter pour revenir au menu precedent ")
        while ((id_biere) not in [self.listbiere[i][0] for i in range (0,len(self.listbiere)-1,1)]) and id_biere != "quitter":
            id_biere=input("cet identifiant n'existe pas, ecrivez quitter pour revenir au menu ou donnez un identifiant existant ")
        if id_biere == "quitter":
            m = 2
        if m == 1:
            self.daobierebrasserie.supprimer(id_biere)
            print("Vous venez d'enlever la biere %s a sa brasserie" % (id_biere))

    def modifier_brasserie(self):
        m=1
        id_brasserie = input("identifiant de la brasserie (ecrivez quitter pour revenir au menu precedent) : ")
        while (id_brasserie not in [self.listbrasseries[i][0] for i in
                                    range(0, len(self.listbrasseries) - 1, 1)]) and id_brasserie != "quitter":
            id_brasserie = input(
                "cet identifiant n'existe pas, ecrivez quitter pour revenir au menu ou donnez un identifiant existant ")
        if id_brasserie == "quitter":
                m = 2
        if m ==1:
            print("Voici la brasserie que vous allez modifier")
            brasserie=Brasseries(*self.daobrasserie.afficher_brasserie(id_brasserie))
            brasserie.afficher()
            label=input("donner le nouveau label de la brasserie. Tapez non si vous ne souhaitez pas le modifier ")
            if label == "non":
                label=brasserie.label
            description=input("donner la nouvelle description de la brasserie. Tapez non si vous ne souhaitez pas le modifier ")
            if description == "non":
                description=brasserie.description
            date_creation = brasserie.date_creation
            date_modification = str(datetime.datetime.now())
            self.daobrasserie.supprimer_brasserie(id_brasserie)
            self.daobrasserie.ajouter_brasserie(id_brasserie,label,description,date_creation,date_modification)
            print("la brasserie %s a ete modifiee" % id_brasserie)

    def nbbiereparbrasserie(self):
        listids=self.listids
        listidsmodif=[]
        listnbbiere=[]
        for id_brasserie in listids:
            nb_biere= int(self.daobierebrasserie.nbbiereparbrasserie(id_brasserie)[0][0])
            listnbbiere=listnbbiere+[nb_biere]
            listidsmodif=listidsmodif + [id_brasserie[0]]
        x = np.arange(len(listidsmodif))
        pylab.bar(x, listnbbiere,align="center")
        pylab.xticks(x,listidsmodif)
        pylab.title('Nombre de biere par brasserie')
        pylab.xlabel('identifiants des brasseries')
        pylab.ylabel('nombre de bieres')
        pylab.show()
















