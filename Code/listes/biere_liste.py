from daobieres import DaoBieres
from daobrasseries import DaoBrasseries
from daobieresbrasseries import DaoBieresBrasseries
from business_object.bieres import Bieres
from brasserie_liste import Brasserieliste
from ingredient_list import Ingredients_liste
from daocategory import DaoCategory
from category import Category
class Biereliste:
    brasserielist= Brasserieliste()
    daobiere = DaoBieres() # Objet Dao
    listbieremodif = DaoBieres().importer_biere() #Utilisation de la methode importation de l'objet
    daobrasserie = DaoBrasseries()
    listebrasseriemodif = daobrasserie.import_ids()
    daobieresbrasserie = DaoBieresBrasseries()
    listebierebrasseriemodif = daobieresbrasserie.importer()
    ingredients = Ingredients_liste()
    daocategorie=DaoCategory()

    def ajouter_biere(self):
        m=1
        id=input("donner l'identifiant de la biere ou ecrivez quitter: ")
        while (id in [self.listbieremodif[i][0] for i in range (0,len(self.listbieremodif)-1,1)]) and id != "quitter": #On verifie l'existence de l'identifiant
            id=input("cet identifiant existe deja, ecrivez quitter pour revenir au menu ou donner un nouvel id ")
        if id == "quitter":
            m=2

        if m == 1:
            nom=input("nom de la biere, enter si vous ne souhaitez pas renseigner cet element : ")
            label=input("donner le label de la biere, appuyez sur entrer si vous ne souhaitez pas renseigner cet element : ")
            description=input("donner la description de la biere, appuyez sur entrer si vous ne souhaitez pas renseigner cet element : ")
            styleid=input("donner le styleid correspondant a votre biere: un entier entre 1 et 175 \n appuyez sur entrer si vous ne souhaitez pas renseigner cet element : ")
            l=['%d' % i for i in range(1, 176, 1)] # On verifie que style id se trouve dans la range decrite
            l=l+[""]
            while styleid not in l:
                styleid=input("donner un entier entre 1 et 175")

            categorieid = input("donner la categorie id correspondant a votre biere: un entier entre 1 et 17, \n appuyez sur entrer si vous ne souhaitez pas renseigner cet element : ")
            L= ['%d' % i for i in range(1, 17, 1)] # On verifie que categorie id se trouve dans les limites decrites
            L=L+[""]
            while categorieid not in L:
                categorieid=input("donner un entier entre 1 et 17") # nb de categories

            brasserie = input("donner l' identifiant de la brasserie dans lesquels votre biere sera servie,appuiez sur entrer si pas de brasseries :")
            self.listebrasseriemodif.append("")
            while brasserie not in self.listebrasseriemodif:
                print("la brasserie n'existe pas ")
                brasserie = input("donner l' identifiant de la brasserie dans lesquels votre biere sera servie, appuiez sur entrer si pas de brasseries :")
            if brasserie != "":
                while (brasserie not in [self.listebrasseriemodif[i][0] for i in
                                        range(0, len(self.listebrasseriemodif) - 1, 1)]) and brasserie != "quitter": ### On regarde si la brasserie existe
                    brasserie = input(
                        "cet identifiant n'existe pas, ecrivez quitter pour revenir au menu ou donnez un identifiant existant ")
                if brasserie != "quitter":
                    self.daobieresbrasserie.ajouter(id,brasserie) #On ajoute a la table d'association biere brasserie

            self.daobiere.ajouter_biere(id,nom,label,description,styleid,categorieid) # On ajoute finalement la biere a la base de donnees
            print("la biere vient d'etre ajoutee a la base de donnees")

    def supprimer_biere(self):
        listbieremodif = DaoBieres().importer_biere()
        id = input("identifiant de la biere ou ecrivez quitter: ")
        L = ["quitter"]
        for i in range(0,len(listbieremodif),1):# On cree une list contenant tout les ids et "quitter"
            L = L+[listbieremodif[i][0]]
        while id not in L:
            print("votre identifiant n'existe pas")
            id = input("identifiant de la biere ou ecrivez quitter: ")
        if id != "quitter":
            self.daobiere.supprimer_biere(id)
            self.daobieresbrasserie.supprimer(id)
            print("la biere vient d'etre supprimee ")

    def afficher_biere_id(self):
        id = input("identifiant de la biere ou ecrivez quitter: ")
        L = ["quitter"]
        for i in range(0, len(self.listbieremodif),1):# On cree une list contenant tout les ids et "quitter"
            L = L + [self.listbieremodif[i][0]]
        while (id) not in L:
            print("votre identifiant n'existe pas")
            id = input("identifiant de la biere ou ecrivez quitter: ")
        if id != "quitter":
            biere = Bieres(*self.daobiere.afficher_biere_id(id))
            biere.afficher()

    def afficher_biere_nom(self):
        nom = input("nom de la biere ou ecrivez quitter : ")
        L = ["quitter"]
        for i in range(0, len(self.listbieremodif),1):# On cree une list contenant tout les ids et "quitter"
            L = L + [self.listbieremodif[i][1]]
        while (nom) not in L:
            print("votre identifiant n'existe pas")
            nom = input("identifiant de la biere ou ecrivez quitter: ")
        if nom != "quitter":
            biere = Bieres(*self.daobiere.afficher_biere_nom(nom))
            biere.afficher()

    def modifier_biere(self):
        id = input("identifiant de la biere ou ecrivez quitter: ")
        L = ["quitter"]
        for i in range(0,len(self.listbieremodif),1):
            L = L+[self.listbieremodif[i][0]]
        while id not in L:
            print("votre identifiant n'existe pas")
            id = input("identifiant de la biere ou ecrivez quitter: ")
        if id != "quitter":
            biere=Bieres(self.daobiere.afficher_biere_id(id))
            print("Voici la biere que vous souhaitez modifier : ")
            biere.afficher()
            print("Nous allons vous demander de renseigner les nouveaux elements de la biere")
            nom=input("donner le nouveau nom de la biere. Si vous ne souhaitez pas le modifier, tapez non ")
            if nom == "non":
                nom=biere.nom
            label=input("donner le nouveau label de la biere. Si vous ne souhaitez pas le modifier, tapez non ")
            if label == "non":
                label=biere.label
            description=input("donner la nouvelle description. Si vous ne souhaitez pas la modifier, tapez non ")
            if description == "non":
                description=biere.description
            styleid=input("donner le nouvel identifiant du style de la biere. Si vous ne souhaitez pas la modifier, tapez non ")
            l=['%d' % i for i in range(1, 176, 1)]# On verifie que style id se trouve dans la range decrite
            l.append("non")
            while styleid not in l:
                styleid=input("donner un entier entre 1 et 175. Si vous ne souhaitez pas la modifier, tapez non ")
            if styleid == "non":
                styleid=biere.styleid
            categorieid = input("donner le nouvel identifiant de la categorie de la biere. Si vous ne souhaitez pas la modifier, tapez non ")
            L= ['%d' % i for i in range(1, 17, 1)] # On verifie que categorie id se trouve dans les limites decrites
            L.append("non")
            while categorieid not in L:
                categorieid=input("donner un entier entre 1 et 17. Si vous ne souhaitez pas la modifier, tapez non ") # nb de categories
            if categorieid == "non":
                categorieid=biere.categorieid
            self.daobiere.supprimer_biere(id)
            self.daobiere.ajouter_biere(id,nom,label,description,styleid,categorieid)
            print("cette biere a ete modifiee")

    def rechercher_biere(self):
        print("Nous allons vous demander des donnees concernant la bière")
        print("si vous ne voulez pas renseigner un element, appuyez sur entree")

        label = input("label de votre biere : ")
        description = input("description de votre biere : ")
        styleid = input("styleid de votre biere : ")
        categorieid = input("categorieid de votre biere : ")
        brasserieid = input("identifiant de la brasserie dans laquelle se trouve votre biere : ")
        self.ingredients.afficher_toutes_categories()
        print("Voici ci dessus toutes les categories d'ingredients a votre disposition")
        ingredientcategorie = input(" categorie d'un ingredient se trouvant dans votre biere : ")
        Request = "" #On cree la commande sql de la requete morceau par morceau
        if label != "":
            Request = Request + "label = '%s'" % label
        if label != "" and (description != "" or styleid != "" or categorieid != "" or brasserieid != "" or ingredientcategorie !=""):
            Request = Request + " , "
        if description != "":
            Request = Request + "description = '%s'" % description
        if description != "" and (styleid != "" or categorieid != "" or brasserieid != "" or ingredientcategorie != ""):
            Request = Request + " , "
        if styleid != "":
            Request = Request + "styleid = '%s'" % styleid
        if styleid != "" and (categorieid != "" or brasserieid != "" or ingredientcategorie != ""):
            Request = Request + " , "
        if categorieid != "":
            Request = Request + "categorieid = '%s'" % categorieid
        if categorieid != "" and (brasserieid != "" or ingredientcategorie != ""):
            Request = Request + " , "
        if ingredientcategorie != "":
            Request = Request + "ingredients.categorie = '%s'" % ingredientcategorie
        if ingredientcategorie != "" and brasserieid != "":
            Request = Request + " , "
        if brasserieid != "":
            Request = Request + "brasserie.id = '%s'" % categorieid
        if Request != "":
            result = self.daobiere.rechercher_biere(Request)  # On demande les resultats de la requete
            if result == [[]] or result == []: #On regarde si il y a des resultats
                print("Aucune biere ne correspond aux criteres de recherche")
            else:
                for i in result: #On affiche les resultats
                    biere = Bieres(*i)
                    biere.afficher()
                    print("\n")
        else:
            print("Aucun critere n'a ete renseigne")

    def afficher_toutes_categorie_biere(self):
        toutescategorie = self.daocategorie.toutes_categories()
        for c in toutescategorie:
            categorie = Category(*c)[0]
            categorie.afficher()












