from daoguildes import DaoGuild
from guilde import Guildes
from daobrasseriesguildes import DaoBrasseriesGuildes

class Guildeliste:
    daoguild = DaoGuild()
    listguildes = daoguild.importer()
    daobrasseriesguildes = DaoBrasseriesGuildes()
    listbrasseriesguildes = daobrasseriesguildes.importer()
    listids=daoguild.import_id()

    def afficher_guilde(self):
        id = input("identifiant de la guilde:")
        L = ["quitter"]
        for i in range(0, len(self.listguildes),1):
            L = L + [self.listguildes[i][0]]
        while (id) not in L:
            print("votre identifiant n'existe pas")
            id = input("identifiant de la guilde ou ecrivez quitter:")
        if id != "quitter":
            guilde = Guildes(*self.daoguild.afficher_guilde(id))
            guilde.afficher()

    def ajouter_guilde(self):
        m=1
        id_guilde = input("identifiant de la guilde, ecrivez quitter si vous souhaitez revenir au menu")
        while ((id_guilde) in [self.listguildes[i][0] for i in
                                    range(0, len(self.listguildes) - 1, 1)]) and id_guilde != "quitter":
            id_guilde = input(
                "cet identifiant existe deja, ecrivez quitter pour revenir au menu ou donnez un identifiant non existant ")
        if id_guilde == "quitter":
            m = 2
        if m ==1:
            nom=input("donner le nom de la guilde, appuyez sur entrer si vous ne souhaitez pas renseigner cet element")
            description=input("donner la description de la guilde, appuyez sur entrer si vous ne souhaitez pas renseigner cet element")
            date_fondation=input("donner la date_fondation de la guilde, appuyez sur entrer si vous ne souhaitez pas renseigner cet element")
            self.daoguild.ajouter_guilde(id_guilde,nom,description,date_fondation)

    def supprimer_guilde(self):
        m = 1
        id_guilde = input("Quelle guilde souhaitez vous supprimer? ecrivez quitter pour revenir au menu precedent")
        while (id_guilde not in [self.listguildes[i][0] for i in
                                    range(0, len(self.listguildes) - 1, 1)]) and id_guilde != "quitter":
            id_guilde = input(
                    "cet identifiant n'existe pas, ecrivez quitter pour revenir au menu ou donnez un identifiant existant ")
        if id_guilde == "quitter":
            m = 2
        if m == 1:
            self.daoguild.supprimer_guilde(id_guilde)