from daobieres import DaoBieres
from bieres import Bieres
from biere_liste import Biereliste
biere_dao = DaoBieres()
bierelist=Biereliste()


def test_create():
    #GIVEN
    b = biere(0000,'nom','label',"description","styleid", "categorieid")
    c_0 = biere_dao.count()

    #WHEN
    create = biere_dao.create(b)

    #THEN
    if biere_dao.count() == c_0+1:
        c_1 = biere_dao.count()
        biere_dao.supprimer_biere(b.id)
    assert c_1 == c_0+1 and create.id == b.id

def test_importer_biere():
    # THEN
    assert biere_dao.importer_biere() != "l'importation a echoue"

def test_afficher_biere():
    #GIVEN
    b = biere(0000,'label',"description","styleid", "categorieid")

    #WHEN
    create = biere_dao.create(b)
    afficher = biere_dao.afficher_biere(b.id)

    #THEN
    if afficher != "l'id donne n'existe pas":
        biere_dao.supprimer_biere(b.id)
    assert afficher != "l'id donne n'existe pas" and afficher.id == b.id

def test_supprimer_biere():
    #GIVEN
    b = biere(0000,'label',"description","styleid", "categorieid")

    #WHEN
    create = biere_dao.create(b)
    c_0 = biere_dao.count()
    supprimer = biere_dao.supprimer_biere(b.id)

    #THEN
    assert biere_dao.count()+1 == c_0 and biere_dao.afficher_biere(b.id) == "l'id donne n'existe pas"

def test_ajouter_biere():
    #GIVEN
    id = 0000
    label = 'label'
    description = "description"
    styleid = "styleid"
    categorieid = "categorieid"
    c_0 = biere_dao.count()

    #WHEN
    create = biere_dao.create(id,label,description,styleid,categorieid)

    #THEN
    if biere_dao.count() == c_0+1:
        c_1 = biere_dao.count()
        biere_dao.supprimer_biere(id)
    assert c_1 == c_0+1 and create.id == id