class Events:

    def __init__(self, id, annee, nom, description, lieu, date,type):
        self.id = id
        self.annee = annee
        self.nom = nom
        self.description = description
        self.lieu = lieu
        self.date =date
        self.type = type

    def __str__(self):
        return '(id=%s, annee=%s, nom=%s, description=%s, lieu=%s,date=%s,type=%s)' % (self.id, self.annee,self.nom, self.description,self.lieu,self.date,self.type)
