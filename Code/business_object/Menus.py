import sys
import ClassesUtilisateurs
import ClassesComptes
import daocomptes
import biere_liste
import brasserie_liste
import guild_list
import ingredient_list

D = daocomptes.DaoCompte()
C = ClassesComptes.Compte()
U_c = ClassesUtilisateurs.Utilisateur_co()
U_d = ClassesUtilisateurs.Utilisateur_deco()

N = "***************************************************************************************************"
M = "##################################################################################################"
L = "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/"
# Ici, on initialise un dictionnaire vide sans lequel on va stocker l'ensemble de nos menus


# ensuite  on code une fonction permettant d'acceder d'un menu a l'autre en fonction de l'input de l'utilisateur
def exec_menu(ch):
    if ch == "1":
        menu['menu_utilisateur_d']()
    elif ch == "2":
        if U_c.connexion()==True:
            print("Vous etes connecte")
            menu['menu_utilisateur_c']()
        else:
            menu['menu_acceuil']()
    elif ch == "3":
        C.creer_compte()
        if C.a == True:
            menu['menu_utilisateur_c']()
        else:
            menu['menu_acceuil']()
    elif ch == "4":
        exit()
    else:
        menu['menu_acceuil']()


# Puis on creer les autres menus necessaires


def menu_utilisateur_d():
    print(L)
    print("Menu utilisateur deconnecte\n")
    print("1. Afficher biere")
    print("2. Afficher brasserie")
    print("3. Afficher guilde")
    print("4. Rechercher biere")
    print("5. Creer compte")
    print("6. Retour")
    print(L)
    ch_d = input("--> ")
    if ch_d == "1":
        B = biere_liste.Biereliste()
        B.afficher_biere_id()
        menu['menu_utilisateur_d']()

    elif ch_d == "2":
        B = brasserie_liste.Brasserieliste()
        B.afficher_brasserie()
        menu['menu_utilisateur_d']()

    elif ch_d == "3":
        B = guild_list.Guildeliste()
        B.afficher_guilde()
        menu['menu_utilisateur_d']()

    elif ch_d == "4":
        B = biere_liste.Biereliste()
        B.rechercher_biere()
        menu['menu_utilisateur_d']()

    elif ch_d == "5":
        U_d = ClassesUtilisateurs.Utilisateur_deco()
        U_d.creer_compte()
        menu['menu_acceuil']()
    elif ch_d == "6":
        menu['menu_acceuil']()

    else:
        print("Ecrivez un nombre entre 1 et 6")
        menu['menu_utilisateur_d']()


def menu_utilisateur_c():
    print(L)
    print("Menu utilisateur connecte\n")
    print("1. Menu Biere")
    print("2. Menu Brasserie")
    print("3. Menu Guilde")
    print("4. Changer mot de passe")
    print("5. Deconnexion")
    print(L)
    ch_c = input("--> ")
    if ch_c == "1":
        menu['menu_biere']()

    elif ch_c == "2":
        menu['menu_brasserie']()

    elif ch_c == "3":
        menu['menu_guilde']()

    elif ch_c == "4":
        C.changer_mdp(U_c.ident)
        menu['menu_utilisateur_c']()

    elif ch_c == "5":
        menu['menu_acceuil']()

    else:
        print("Entrer un chiffre en 1 et 5")
        menu['menu_utilisateur_c']()

def exit():
    sys.exit()

def menu_affichage_biere():
    B = biere_liste.Biereliste()
    print("1. Afficher en fonction de l'id")
    print("2. Afficher en fonction du nom")
    print("3. Retour")
    cmd = input("--> ")
    if cmd == "1":
        B.afficher_biere_id()
        menu['menu_biere']()
    elif cmd == "2":
        B.afficher_biere_nom()
        menu['menu_biere']()
    elif cmd == "3":
        menu['menu_biere']()
    else:
        menu['menu_affichage_biere']()


#On cree les menus metiers pour pour les utilisateurs connectes
def menu_biere():
    print("1. Afficher biere")
    print("2. Rechercher biere")
    print("3. Ajouter biere")
    print("4. Supprimer biere")
    print("5. Modifier biere")
    print("6. Ajouter une biere à une brasserie")
    print("7. Afficher tous les ingredients d'une categorie")
    print("8. Afficher toutes les categories d'ingredients")
    print("9. Retour")
    ch_c = input("--> ")
    B = biere_liste.Biereliste()
    if ch_c == "1":
        menu['menu_affichage_biere']()

    elif ch_c == "2":
        B.rechercher_biere()
        menu['menu_biere']()

    elif ch_c == "3":
        B.ajouter_biere()
        menu['menu_biere']()

    elif ch_c == "4":
        B.supprimer_biere()
        menu['menu_biere']()

    elif ch_c == "5":
        B.modifier_biere()
        menu['menu_biere']()

    elif ch_c == "6":
        D = brasserie_liste.Brasserieliste()
        D.ajouter_bierebrasserie()
        menu['menu_biere']()

    elif ch_c == "7":
        D = ingredient_list.Ingredients_liste()
        D.afficher_ingredient_categorie()
        menu['menu_biere']()

    elif ch_c == "8":
        D = ingredient_list.Ingredients_liste()
        D.afficher_toutes_categories()
        menu['menu_biere']()

    elif ch_c == "9":
        menu['menu_utilisateur_c']()

    else:
        print("Entrer un chiffre en 1 et 9")
        menu['menu_biere']()


def menu_brasserie():
    print("1. Afficher brasserie")
    print("2. Ajouter brasserie")
    print("3. Supprimer brasserie")
    print("4. Modifier brasserie")
    print("5. Ajouter une biere à une brasserie")
    print("6. Supprimer une biere d'une brasserie")
    print("7. Afficher le nombre de bieres par brasserie")
    print("8. Retour")
    ch_c = input("--> ")
    D = brasserie_liste.Brasserieliste()
    if ch_c == "1":
        D.afficher_brasserie()
        menu['menu_brasserie']()

    elif ch_c == "2":
        D.ajouter_brasserie()
        menu['menu_brasserie']()

    elif ch_c == "3":
        D.supprimer_brasserie()
        menu['menu_brasserie']()

    elif ch_c == "4":
        D.modifier_brasserie()
        menu['menu_brasserie']()

    elif ch_c == "5":
        D.ajouter_bierebrasserie()
        menu['menu_brasserie']()

    elif ch_c == "6":
        D.supprimer_bierebrasserie()
        menu['menu_brasserie']()

    elif ch_c == "7":
        D.nbbiereparbrasserie()
        menu['menu_brasserie']()

    elif ch_c == "8":
        menu['menu_utilisateur_c']()

    else:
        print("Entrer un chiffre en 1 et 8")
        menu['menu_brasserie']()


def menu_guilde():
    print("1. Afficher une guilde")
    print("2. Ajouter une guilde")
    print("3. Supprimer une guilde")
    print("4. Retour")
    ch_c = input("--> ")

    if ch_c == "1":
        G = guild_list.Guildeliste()
        G.afficher_guilde()
        menu['menu_guilde']()

    elif ch_c == "2":
        G = guild_list.Guildeliste()
        G.ajouter_guilde()
        menu['menu_guilde']()

    elif ch_c == "3":
        G = guild_list.Guildeliste()
        G.supprimer_guilde()
        menu['menu_guilde']()

    elif ch_c == "4":
        menu['menu_utilisateur_c']()

    else:
        print("ecrivez un nombre entre 1 et 4")
        menu['menu_guilde']()

# On commence par creer notre menu d'acceuil
def menu_acceuil():
    print(M)
    print("Bienvenue dans notre application Brewery DB")
    print(M)
    print()
    print(M)
    print("Menu d'acceuil\n")
    print("1. Consulter")
    print("2. Connexion")
    print("3. Creer compte")
    print("4. Quitter")
    print("Que voulez vous faire ?")
    print("Veuillez ecrire un nombre entre 1 et 4")
    print(M)
    ch = input("--> ")
    exec_menu(ch)

menu = {'menu_acceuil': menu_acceuil,
        'menu_utilisateur_d': menu_utilisateur_d,
        'menu_utilisateur_c': menu_utilisateur_c,
        'menu_biere' : menu_biere,
        'menu_brasserie' : menu_brasserie,
        'menu_guilde' : menu_guilde,
        'menu_affichage_biere': menu_affichage_biere}



menu_acceuil()
#menu_retour_utilisateur_c()
#menu_retour_utilisateur_d()