# On creer ici la classe mere utilisateur
import ClassesComptes
import daocomptes
import hashlib

# Ensuite, on code les classes filles qui heritent de la classe utilisateur
class Utilisateur_co():
    C = ClassesComptes.Compte()
    D = daocomptes.DaoCompte()
    def __init__(self, ident="", mdp=""):
        self.ident=ident
        self.mdp = mdp

    def connexion(self):
        print('Saisissez votre identifiant')
        ch_ident = input()
        self.ident = ch_ident
        if not(self.C.compte_existant(ch_ident)):
            print("Ce compte n'existe pas")
            return (False)
        else:
            print('Saisissez votre mot de passe')
            ch_mdp = input()
            if (self.D.connexion(ch_ident) != hashlib.sha1(ch_mdp.encode()).hexdigest()):
                print("mauvais mot de passe")
                return False
            else:
                return True # Ici les majuscules sont prisent en compte dans le mdp


class Utilisateur_deco():
    C = ClassesComptes.Compte()
    def __init__(self, ident=""):
        self.ident=ident

    def creer_compte(self):
        self.C.creer_compte()