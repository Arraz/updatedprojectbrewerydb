#La base de donnees des comptes sous postegreSQL est "account"
import hashlib
import daocomptes
from fonction_utile import integer
class Compte:
    D= daocomptes.DaoCompte()
    def __init__(self,ident="",mdp=""):
        self.ident = ident
        self.mdp= mdp

    def compte_existant(self,ident):
        return(self.D.compte_existant(ident) != 0)

    def ajouter_compte(self,ident,mdp):
        C=Compte()
        if not(C.compte_existant(ident)):
            self.D.ajouter_compte(ident,mdp)
        else:
            print("L'identifiant est deja utilise")

    def creer_compte(self):
        self.a=False
        print("Veuillez saisir un identifiant : ")
        print("un identifiant est un nombre" )
        ch_ident = integer()
        C=Compte()
        if C.compte_existant(ch_ident):
            print()
            print("L'identifiant est deja utilise\n")
        else:
            print("Saisissez un mot de passe : ")
            ch_mdp1 = input()
            print("Confirmez votre mot de passe : ")
            ch_mdp2 = input()
            if ch_mdp1 != ch_mdp2:
                print()
                print("Les mots de passe sont differents\n")
            else:
                c = Compte(ch_ident,hashlib.sha1(ch_mdp1.encode()).hexdigest())
                C.ajouter_compte(c.ident,c.mdp)
                self.a=True
                print("Votre compte a ete cree\n")

    def supprimer_compte(self,ident):
        C=Compte()
        if C.compte_existant(ident):
            self.D.supprimer_compte(ident)
            print("Le compte a ete supprime")
        else:
            print("Le compte n'existe pas")

    def changer_mdp(self,ident): #Cette fonction ne releve pas d'erreur lorsque nouveau_mdp = ancien_mdp
        C=Compte()
        mdp=input("Veuillez taper le nouveau mot de passe : ")
        if C.compte_existant(ident):
            self.D.supprimer_compte(ident)
            self.D.ajouter_compte(ident,hashlib.sha1(mdp.encode()).hexdigest())
            print("Votre mot de passe a ete change")
        else:
            print("Le compte n'existe pas")

